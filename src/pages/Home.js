import React, { useState, useEffect } from "react";
import { NavLink } from "react-router-dom";
import {qlPhimService} from "../services/QuanLyPhimService"

const Home = (props) => {
  let [danhSachPhim, setDanhSachPhim] = useState([]);

  const renderDanhSachPhim = () => {
    return danhSachPhim.map((phim, index) => {
      return (
        <div className="col-4" key={index}>
          <div className="card text-left">
            <img
              className="card-img-top"
              src={phim.hinhAnh}
              alt={phim.maPhim}
            />
            <div className="card-body">
              <h4 className="card-title">{phim.tenPhim}</h4>
              <NavLink to={`/moviedetail/${phim.maPhim}`} className="btn btn-success">Đặt vé</NavLink>
            </div>
          </div>
        </div>
      );
    });
  };

  //UseEffect thay thế cho 3 lifecycle
  //Tham số 2 [] rỗng sử dụng như componentdidmount
  useEffect(() => {
    qlPhimService
      .layDanhSachPhim()
      .then((result) => {
        setDanhSachPhim(result.data);
      })
      .catch((errors) => {
        console.log(errors.response.data);
      });
  }, []);

  return (
    <div className="container">
      <div className="display-4">Danh sách phim</div>
      <div className="row">{renderDanhSachPhim()}</div>
    </div>
  );
};

export default Home;
