import React, { useState } from "react";
import { qlNguoiDung } from "../services/QuanLyNguoiDung";
import { userLogin, token } from "../config/settings";
import { useDispatch } from "react-redux";
import { dangNhapAction } from "../redux/actions/quanLyNguoiDungAcion";
//UseState là thay cho this.state tương ứng rcc

const Login = (props) => {
  const dispatch = useDispatch();

  let [state, setState] = useState({
    values: {
      taiKhoan: "",
      matKhau: "",
    },
    errors: {
      taiKhoan: "",
      matKhau: "",
    },
  });

  let [result, setResult] = useState("Chưa submit");

  const handleChangeInput = (event) => {
    let { value, name } = event.target;

    //Tạo ra Object this.state.values mới
    let newValues = {
      ...state.values,
      [name]: value,
    };
    //Tạo ra Object this.state.errors mới
    //Xét trường hợp rỗng
    let newErrors = {
      ...state.errors,
      [name]: value === "" ? "(*) Không được bỏ trống !" : "",
    };

    //setState lại values và errors
    setState({ values: newValues, errors: newErrors });
  };

  const handleSubmit = (event) => {
    event.preventDefault();

    //Sử dụng api đăng nhập
    qlNguoiDung
      .dangNhap(state.values)
      .then((res) => {
        //Lưu thông tin user vào local storage
        localStorage.setItem(userLogin, JSON.stringify(res.data));
        //Lưu thông tin token vào localstorage
        localStorage.setItem(token, res.data.accessToken);
        dispatch(dangNhapAction(res.data.taiKhoan));
        props.history.push("/home");
      })
      .catch((error) => {
        console.log(error.response.data);
      });
    setResult("Đăng nhập thành công! ");
  };

  return (
    <div className="container">
      <form onSubmit={handleSubmit}>
        <h3>{result}</h3>
        <h3 className="display-4">Đăng nhập</h3>
        <div className="form-group">
          <span>Tài khoản</span>
          <input
            name="taiKhoan"
            className="form-control"
            onChange={handleChangeInput}
          />
          <span className="text-danger">{state.errors.taiKhoan}</span>
        </div>
        <div className="form-group">
          <span>Mật khẩu</span>
          <input
            name="matKhau"
            type="password"
            className="form-control"
            onChange={handleChangeInput}
          />
          <span className="text-danger">{state.errors.matKhau}</span>
        </div>
        <div className="form-group">
          <button className="btn btn-success">Đăng nhập</button>
        </div>
      </form>
    </div>
  );
};

export default Login;
