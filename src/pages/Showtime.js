import React, { Fragment, useState, useEffect } from "react";

import { qlPhimService } from "../services/QuanLyPhimService";
import { qlNguoiDung } from "../services/QuanLyNguoiDung";
import { userLogin } from "../config/settings";
import { Redirect } from "react-router-dom";

export default function Showtime(props) {
  let [thongTinPhongVe, setThongTinPhongVe] = useState({});

  let [danhSachGheDangDat, setDanhSachGheDangDat] = useState([]);

  useEffect(() => {
    //Lấy mã lịch chiếu từ param url
    let { maLichChieu } = props.match.params;
    qlPhimService
      .layThongTinPhongVe(maLichChieu)
      .then((res) => {
        console.log(res.data);
        setThongTinPhongVe(res.data);
      })
      .catch((err) => {
        console.log(err.response.data);
      });
  }, []);
  //Tương đương componentDidMount

  const renderDanhSachGhe = () => {
    let { danhSachGhe } = thongTinPhongVe;
    return danhSachGhe?.map((ghe, index) => {
      return (
        <Fragment key={index}>
          {renderGhe(ghe.daDat, ghe)} {(index + 1) % 16 === 0 ? <br /> : ""}
        </Fragment>
      );
    });
  };

  const datGhe = (ghe) => {
    //Nếu như mảng ghé đang đặt chưa có ghế đó => push vào mảng
    let index = danhSachGheDangDat.findIndex(
      (gheDangDat) => gheDangDat.stt === ghe.stt
    );
    if (index !== -1) {
      //Nếu click vào ghế đã có trang mảng ghế đang đặt thì remove ra
      danhSachGheDangDat.splice(index, 1);
    } else {
      //Nếu như click vào chưa c1o trong mảng ghế đang đặt thì push vào
      danhSachGheDangDat = [...danhSachGheDangDat, ghe];
    }
    //Set lại state danhSachGheDangDat và render lại giao diện
    setDanhSachGheDangDat([...danhSachGheDangDat]);
  };
  console.log("danhSachGheDangDat", danhSachGheDangDat);

  const datVe = () => {
    let thongTinDatVe = {
      maLichChieu: props.match.params.maLichChieu,
      danhSachVe: danhSachGheDangDat,
      taiKhoanNguoiDung: JSON.parse(localStorage.getItem("userLogin")).taiKhoan,
    };
    qlNguoiDung
      .datVe(thongTinDatVe)
      .then((res) => {
        console.log(res.data);
        window.location.reload();
      })
      .catch((err) => {
        console.log(err.response.data);
      });
  };

  const renderGhe = (daDat, ghe) => {
    if (daDat) {
      return (
        <button className={"ghe gheDaDat"} disabled>
          X
        </button>
      );
    } else {
      //Kiểm tra ghế có đang đặt hay không
      let cssGheDangDat = "";
      let index = danhSachGheDangDat.findIndex(
        (gheDangDat) => gheDangDat.stt === ghe.stt
      );
      if (index !== -1) {
        //Nếu ghế trong 160 ghế có trong ghế đang đặt => render class ghế đó màu xanh
        cssGheDangDat = "gheDangDat";
      }

      //Xét ghế vip
      let cssGheVip = "";
      if (ghe.loaiGhe === "Vip") {
        cssGheVip = "gheVip";
      }

      return (
        <button
          onClick={() => {
            datGhe(ghe);
          }}
          className={`ghe ${cssGheVip} ${cssGheDangDat}`}
        >
          {ghe.stt}
        </button>
      );
    }
  };

  const renderThongTinPhim = () => {
    return (
      <div className="container">
        <h1 className="display-4">{thongTinPhongVe.thongTinPhim?.tenPhim}</h1>
        <img
          style={{ width: 200, height: 300 }}
          src={thongTinPhongVe.thongTinPhim?.hinhAnh}
        />
        <h3>
          {thongTinPhongVe.thongTinPhim?.tenCumRap} -{" "}
          {thongTinPhongVe.thongTinPhim?.tenRap}
        </h3>
        <p>{thongTinPhongVe.thongTinPhim?.diaChi}</p>
        <p>
          Ngày chiếu: {thongTinPhongVe.thongTinPhim?.ngayChieu} - Giờ chiếu:{" "}
          {thongTinPhongVe.thongTinPhim?.gioChieu}
        </p>
        <div>
          {/* Render thông tin ghé đang đặt */}
          {renderThongTinGheDangDat()}
          <button
            onClick={() => {
              datVe();
            }}
            className="w-100 btn btn-success"
          >
            ĐẶT VÉ
          </button>
        </div>
      </div>
    );
  };

  const renderThongTinGheDangDat = () => {
    return (
      <div>
        {danhSachGheDangDat.map((gheDangDat, index) => {
          return <span key={index}>Ghế {gheDangDat.tenGhe}</span>;
        })}{" "}
        -{" "}
        {danhSachGheDangDat
          .reduce((tongTien, gheDangDat, index) => {
            return (tongTien += gheDangDat.giaVe);
          }, 0)
          .toLocaleString()}
      </div>
    );
  };

  if (!localStorage.getItem(userLogin)) {
    alert("Đăng nhập để đặt vé ! ");
    return <Redirect to="/login" />;
  }

  return (
    <div className="container-fluid">
      <div className="row">
        <div className="col-7 text-center mb-5 mt-5">
          <div className="trapezoid "></div>
          {renderDanhSachGhe()}
        </div>
        <div className="col-5">{renderThongTinPhim()}</div>
      </div>
    </div>
  );
}
