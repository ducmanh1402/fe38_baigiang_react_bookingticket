import React, { Component } from "react";

class Register extends Component {
  state = {
    values: {
      hoTen: "",
      taiKhoan: "",
      matKhau: "",
      email: "",
      soDienThoai: "",
    },
    errors: {
      hoTen: "",
      taiKhoan: "",
      matKhau: "",
      email: "",
      soDienThoai: "",
    },
  };

  handleChangeInput = (event) => {
    let { value, name } = event.target;

    //Tạo ra Object this.state.values mới
    let newValues = {
      ...this.state.values,
      [name]: value,
    };
    //Tạo ra Object this.state.errors mới
    //Xét trường hợp rỗng
    let newErrors = {
      ...this.state.errors,
      [name]: value === "" ? "(*) Không được bỏ trống !" : "",
    };

    //Xét email
    if (name === "email") {
      let regexEmail = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
      if (value.match(regexEmail)) {
        newErrors.email = "";
      } else {
        newErrors.email = "(*)Email không hợp lệ !";
      }
    }

    //setState lại values và errors
    this.setState({ values: newValues, errors: newErrors });
  };

  handleSubmit = (event) => {
    //Chặn sự kiện load lại trang từ form submit
    event.preventDefault();
    let valid = true;
    let { values, errors } = this.state;
    for (let key in values) {
      //Nếu như có 1 values bằng rỗng thì không hợp lệ
      if (values[key] === "") {
        valid = false;
      }
      //Check errors
      //Nếu như có 1 errors != rỗng => còn lỗi
      for (let key in errors) {
        if (errors[key] !== "") {
          valid = false;
        }
      }
      if (!valid) {
        alert("Thông tin không hợp lệ !");
        return;
      }
      //Gọi api hoặc dispatch redux
    }
  };

  render() {
    return (
      <div>
        <form className="container" onSubmit={this.handleSubmit}>
          <h3 className="display-4">Register</h3>
          <div className="form-group">
            <span>Họ tên</span>
            <input
              className="form-control"
              name="hoTen"
              onChange={this.handleChangeInput}
            />
            <span className="text-danger">{this.state.errors.hoTen}</span>
          </div>
          <div className="form-group">
            <span>Tài khoản</span>
            <input
              className="form-control"
              name="taiKhoan"
              onChange={this.handleChangeInput}
            />
            <span className="text-danger">{this.state.errors.taiKhoan}</span>
          </div>
          <div className="form-group">
            <span>Mật khẩu</span>
            <input
              type="password"
              className="form-control"
              name="matKhau"
              onChange={this.handleChangeInput}
            />
            <span className="text-danger">{this.state.errors.matKhau}</span>
          </div>
          <div className="form-group">
            <span>Email</span>
            <input
              className="form-control"
              name="email"
              onChange={this.handleChangeInput}
            />
            <span className="text-danger">{this.state.errors.email}</span>
          </div>
          <div className="form-group">
            <span>Số điện thoại</span>
            <input
              className="form-control"
              name="soDienThoai"
              onChange={this.handleChangeInput}
            />
            <span className="text-danger">{this.state.errors.soDienThoai}</span>
          </div>
          <div className="form-group">
            <button type="submit" className="btn btn-success">
              Đăng kí
            </button>
          </div>
        </form>
      </div>
    );
  }
}

export default Register;
