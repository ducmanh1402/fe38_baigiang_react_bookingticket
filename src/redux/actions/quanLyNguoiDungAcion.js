import { DANG_NHAP } from "../types/quanLyNguoiDungType";

export const dangNhapAction = (taiKhoan) => {
  return {
    type: DANG_NHAP,
    taiKhoan,
  };
};
